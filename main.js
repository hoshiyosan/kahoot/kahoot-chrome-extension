/*
join utils
*/
const chars = "abcdefghijklmnopqrstuvwxyz1234567890";
const alpha = chars;

function randint(a, b) {
	return Math.floor(
		Math.random() * (b - a) + a
	);
}

function randchar(alpha) {
	const n = alpha.length;
	return alpha[randint(0, n)];
}

function randomName() {
	const size = randint(4, 10);
	let name = "";
	for (let i = 0; i < size; i++) {
		name += randchar(alpha);
	}
	return name;
}

function joinKahoot() {
	let nickname = document.getElementById('nickname');
	nickname.value = randomName();
	setTimeout(function () {
		document.querySelector('button[type=submit]').click();
	}, 1000);
}

/*
controllers
*/

let current_page = null;

function pageChanged() {
	if (window.location.href !== current_page) {
		current_page = window.location.href;
		return true;
	} else {
		return false;
	}
}

function mainloop() {
	console.log('loop');
	if (pageChanged() === true) {
		console.log('page changed');

		switch (current_page) {
			case "https://kahoot.it/v2/join":
				console.log('join kahoot');
				joinKahoot();
				break;
		}
	}
}

setInterval(mainloop, 100);