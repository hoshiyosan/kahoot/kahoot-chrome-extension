let current_page = null;


function navigate(page) {
	let pages = document.querySelectorAll('*[page]');
	for (let i = 0; i < pages.length; i++) {
		pages[i].style.display = (pages[i].getAttribute('page') == page) ? 'block' : 'none';
	}

	current_page = page;
	bindListeners(current_page)
}


function bindListeners(page) {
	let listeners = {
		'home': () => {
			document.getElementById('start-random-game').addEventListener(
				'click', () => {
					navigate('normal-game')
				})
		}
	} [page];

	// bind listeners if defined
	if (listeners !== undefined) {
		listeners();
	}
}


(function () {
	navigate('home');
})()